//
//  Location.swift
//  FortGlaksoWatch WatchKit Extension
//
//  Created by Konrad on 24/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

class Location {
    var latitude: Double?
    var longitude: Double?
    var timestamp: String?
    
}
