//
//  LocationManager.swift
//  FortGlaksoWatch WatchKit Extension
//
//  Created by Konrad on 24/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

import CoreLocation
import Foundation

class LocationManager {
    static let shared = CLLocationManager()
    
    private init() {}
}
