//
//  InterfaceController.swift
//  FortGlaksoWatch WatchKit Extension
//
//  Created by Mikolaj Bujok on 24/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import CoreData
import MapKit
import WatchKit
import CoreLocation
import Foundation


class InterfaceController: WKInterfaceController {

    
    private let locationManager = LocationManager.shared
    private var locationList: [CLLocation] = []
    private var locations = [Location]()
    private var timer: Timer?
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // Configure interface objects here.
        startLocationUpdates()
        locationManager.requestWhenInUseAuthorization()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        startLocationUpdates()
        locationManager.requestWhenInUseAuthorization()
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
            
            // Do any additional setup after loading the view.
        
        private func startLocationUpdates() {
            locationManager.delegate = self
            locationManager.activityType = .fitness
            locationManager.distanceFilter = 10
            locationManager.startUpdatingLocation()
        }
    
    
    @IBAction func stopUpdatingButton() {
        locationManager.stopUpdatingLocation()
        print(locations)
    }
    
    }
    
    extension InterfaceController: CLLocationManagerDelegate {
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            for newLocation in locations {
                let howRecent = newLocation.timestamp.timeIntervalSinceNow
                guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else { continue }
                locationList.append(newLocation)
            }
        }
    }



