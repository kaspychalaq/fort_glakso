//
//  ViewController.swift
//  FortGlaksoWatch
//
//  Created by Mikolaj Bujok on 24/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let locationNetworkManager = LocationNetworkManager.shared
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        locationManager.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        let loc = Location()
        loc.latitude = 10
        loc.longitude = 10
        loc.timestamp = "1940-01-01 00:00:00"
        locationNetworkManager.postLocation(location: loc, parentId: 1, date: loc.timestamp!, successHandler: self.success, errorHandler: self.errorHand)
    }
    func success() -> Void {
        print("success")
    }
    func errorHand(error: Error?) -> Void {
        print("error")
    }


}


