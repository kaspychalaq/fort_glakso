//
//  LocationNetworkHelper.swift
//  FortGlaksoWatch
//
//  Created by Konrad on 25/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

class LocationNetworkManager {
    static let shared = LocationNetworkManager()
    
    func postLocation(location: Location, parentId: Int, date: String, successHandler: @escaping () -> Void, errorHandler: @escaping (_ error: Error?) -> Void) {
        if let url = URL(string: "http://kiniak94-001-site1.ctempurl.com/api/locations") {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let json: [String: Any] = ["PatientId": parentId, "Latitude": location.latitude!, "Longitude": location.longitude!, "Date": date]
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            request.httpBody = jsonData
            let session = URLSession.shared

            session.dataTask(with: request) { data, response, error in
                if let error = error {
                    DispatchQueue.main.async {
                        errorHandler(error)
                    }
                }
                if let response = response as? HTTPURLResponse {
                    if response.statusCode == 200 {
                        DispatchQueue.main.async {
                            print("good")
                        }
//                        if let data = data {
//                            do {
//                                
//                            } catch {
//
//                            }
//                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        errorHandler(nil)
                    }
                }

        }
    }
    }
    
//    func requestForToken(login: String, password: String, successHandler: @escaping () -> Void, errorHandler: @escaping (_ error: Error) -> Void) {
//
//            let session = URLSession.shared
//            //            sleep(10)
//            session.dataTask(with: request) { data, response, error in
//                if let error = error {
//                    DispatchQueue.main.async {
//                        //                        errorHandler(error.localizedDescription)
//                        errorHandler(error)
//                    }
//                }
//                //                sleep(10)
//                if let response = response as? HTTPURLResponse {
//                    if response.statusCode == 200 {
//                        if let data = data {
//                            do {
//                                let pingResponse = try JSONDecoder().decode(PingResponse.self, from: data)
//                                //                                DispatchQueue.global().async {
//                                DispatchQueue.main.async {
//                                    successHandler()
//                                    print("successHandl")
//                                }
//
//                            } catch {
//                                print(error)
//                                errorHandler(error)
//                            }
//                        }
//                    } else {
//                        errorHandler(error)
//                    }
//                }
//                }.resume()
//        }
}


