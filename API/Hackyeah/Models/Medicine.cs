﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hackyeah.Models
{
    public class Medicine
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int PatientId { get; set; }
        public TimeSpan Time { get; set; }
        public int Dose { get; set; }
    }
}