﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hackyeah.Models
{
    public class DBConnection : DbContext
    {
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Carer> Carers { get; set; }
        public DbSet<PatientsCarer> PatientsCarers { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<Dispenser> Dispensers { get; set; }
    }
}