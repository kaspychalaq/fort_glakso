﻿using Hackyeah.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hackyeah.Controllers
{
    public class AddressesController : ApiController
    {
        private DBConnection DB = new DBConnection();

        //GET /api/addresses
        public HttpResponseMessage GetAddresses()
        {
            List<Address> addressList = new List<Address>();
            addressList = DB.Addresses.ToList();
            HttpResponseMessage response;
            response = Request.CreateResponse(HttpStatusCode.OK, addressList);

            return response;
        }

        //GET /api/addresses/{id}
        public HttpResponseMessage GetLocationById(int id)
        {
            List<Address> addressList = new List<Address>();
            addressList = DB.Addresses.ToList();
            HttpResponseMessage response;

            if (addressList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, addressList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Location not found.");
            }

            return response;
        }

        //GET /api/addresses?patientId={patientId}
        public HttpResponseMessage GetAddressByPatientId(int patientId)
        {
            List<Address> addressList = new List<Address>();
            addressList = DB.Addresses.Where(p => p.PatientId == patientId).ToList();
            HttpResponseMessage response;


            if (addressList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, addressList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Location for user not found.");
            }

            return response;
        }

        //POST /api/addresses
        [HttpPost]
        public HttpResponseMessage AddNewAddress(Address adress)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DB.Addresses.Add(adress);
                    DB.SaveChanges();

                    var response = Request.CreateResponse(HttpStatusCode.Created, adress);
                    response.Headers.Location = Request.RequestUri;

                    return response;
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, adress);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.InnerException);
            }
        }
    }
}