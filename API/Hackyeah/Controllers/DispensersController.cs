﻿using Hackyeah.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hackyeah.Controllers
{
    public class DispensersController : ApiController
    {
        private DBConnection DB = new DBConnection();

        //GET /api/dispensers
        public HttpResponseMessage GetDispensers()
        {
            List<Dispenser> dispenserList = new List<Dispenser>();
            dispenserList = DB.Dispensers.ToList();
            HttpResponseMessage response;
            response = Request.CreateResponse(HttpStatusCode.OK, dispenserList);

            return response;
        }

        //GET /api/dispensers/{id}
        public HttpResponseMessage GetdDispensersById(int id)
        {
            List<Dispenser> dispenserList = new List<Dispenser>();
            dispenserList = DB.Dispensers.Where(p => p.Id == id).ToList();
            HttpResponseMessage response;

            if (dispenserList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, dispenserList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Patient not found.");
            }

            return response;
        }

        //GET /api/dispensers?patientId={patientId}
        public HttpResponseMessage GetDispensersByPatient(int patientId)
        {
            List<Dispenser> dispenserList = new List<Dispenser>();
            dispenserList = DB.Dispensers.Where(p => p.PatientId == patientId).ToList();
            HttpResponseMessage response;


            if (dispenserList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, dispenserList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Dispenser for user not found.");
            }

            return response;
        }

        //POST /api/dispensers
        [HttpPost]
        public HttpResponseMessage AddNewDispenser(Dispenser dispenser)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DB.Dispensers.Add(dispenser);
                    DB.SaveChanges();

                    var response = Request.CreateResponse(HttpStatusCode.Created, dispenser);
                    response.Headers.Location = Request.RequestUri;

                    return response;
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, dispenser);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.InnerException);
            }
        }
    }
}