﻿using Hackyeah.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hackyeah.Controllers
{
    public class PatientsCarersController : ApiController
    {
        private DBConnection DB = new DBConnection();

        //GET /api/patientsCarers
        public HttpResponseMessage GetPatients()
        {
            List<PatientsCarer> patientsCarersList = new List<PatientsCarer>();
            patientsCarersList = DB.PatientsCarers.ToList();
            HttpResponseMessage response;
            response = Request.CreateResponse(HttpStatusCode.OK, patientsCarersList);

            return response;
        }
        //GET /api/patientsCarers?carerId={carerId}
        public HttpResponseMessage GetPatientsByCarerId(int carerId)
        {
            List<PatientsCarer> patientsCarersList = new List<PatientsCarer>();
            patientsCarersList = DB.PatientsCarers.Where(p => p.CarerId == carerId).ToList();
            HttpResponseMessage response;

            if (patientsCarersList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, patientsCarersList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Patient not found.");
            }

            return response;
        }

        //GET /api/patientsCarers?patientId={patientId}
        public HttpResponseMessage GetCarersbyPatientId(int patientId)
        {
            List<PatientsCarer> patientsCarersList = new List<PatientsCarer>();
            patientsCarersList = DB.PatientsCarers.Where(p => p.PatientId == patientId).ToList();
            HttpResponseMessage response;

            if (patientsCarersList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, patientsCarersList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Carer not found.");
            }

            return response;
        }

        //POST /api/patientsCarers
        [HttpPost]
        public HttpResponseMessage AddNewPatient(PatientsCarer patientsCarer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DB.PatientsCarers.Add(patientsCarer);
                    DB.SaveChanges();

                    var response = Request.CreateResponse(HttpStatusCode.Created, patientsCarer);
                    response.Headers.Location = Request.RequestUri;

                    return response;
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, patientsCarer);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.InnerException);
            }
        }
    }
}