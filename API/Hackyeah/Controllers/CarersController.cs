﻿using Hackyeah.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hackyeah.Controllers
{
    public class CarersController : ApiController
    {
        private DBConnection DB = new DBConnection();

        //GET /api/carers
        public HttpResponseMessage GetCarers()
        {
            List<Carer> carerList = new List<Carer>();
            carerList = DB.Carers.ToList();
            HttpResponseMessage response;
            response = Request.CreateResponse(HttpStatusCode.OK, carerList);

            return response;
        }

        //GET /api/carers/{id}
        public HttpResponseMessage GetCarerById(int id)
        {
            List<Carer> carerList = new List<Carer>();
            carerList = DB.Carers.Where(p => p.Id == id).ToList();
            HttpResponseMessage response;

            if (carerList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, carerList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Patient not found.");
            }

            return response;
        }

        //GET /api/carers?userName={userName}&password={password}
        public HttpResponseMessage GetCarer(string userName, string password)
        {
            List<Carer> carerList = new List<Carer>();
            carerList = DB.Carers.Where(p => (p.UserName == userName && p.Password == password)).ToList();
            HttpResponseMessage response;

            

            if (carerList.Count() > 0)
            {
                var result = new { Status = 200, Message = "OK" };
                response = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                var result = new { Status = 404, Message = "Not found." };
                response = Request.CreateResponse(HttpStatusCode.NotFound, result);
            }

            return response;
        }

        //POST /api/carers
        [HttpPost]
        public HttpResponseMessage AddNewCarer(Carer carer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DB.Carers.Add(carer);
                    DB.SaveChanges();

                    var response = Request.CreateResponse(HttpStatusCode.Created, carer);
                    response.Headers.Location = Request.RequestUri;

                    return response;
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, carer);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.InnerException);
            }
        }
    }
}