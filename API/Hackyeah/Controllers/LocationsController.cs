﻿using Hackyeah.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.ModelBinding;

namespace Hackyeah.Controllers
{
    public class LocationsController : ApiController
    {
        private DBConnection DB = new DBConnection();

        //GET /api/locations
        public HttpResponseMessage GetLocations()
        {
            List<Location> locationList = new List<Location>();
            locationList = DB.Locations.ToList();
            HttpResponseMessage response;
            response = Request.CreateResponse(HttpStatusCode.OK, locationList);

            return response;
        }

        //GET /api/locations/{id}
        public HttpResponseMessage GetLocationById(int id)
        {
            List<Location> locationList = new List<Location>();
            locationList = DB.Locations.Where(p=>p.Id == id).ToList();
            HttpResponseMessage response;
            

            if (locationList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, locationList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Location not found.");
            }

            return response;
        }

        //GET /api/locations?patientId={patientId}
        public HttpResponseMessage GetLocationByUserId(int patientId)
        {
            List<Location> locationList = new List<Location>();
            locationList = DB.Locations.Where(p => p.PatientId == patientId).ToList();
            HttpResponseMessage response;


            if (locationList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, locationList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Location for user not found.");
            }

            return response;
        }

        //GET /api/locations?patientId={patientId}&startDate={startDate}
        public HttpResponseMessage GetLocationsByUserIdAndTime(int patientId, DateTime startDate)
        {
            List<Location> locationList = new List<Location>();
            locationList = DB.Locations.Where(p => (p.PatientId == patientId) && (p.Date > startDate)).ToList();
            HttpResponseMessage response;


            if (locationList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, locationList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Location for user not found.");
            }

            return response;
        }

        //POST /api/locations
        [HttpPost]
        public HttpResponseMessage AddNewLocation(Location location)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DB.Locations.Add(location);
                    DB.SaveChanges();

                    var response = Request.CreateResponse(HttpStatusCode.Created, location);
                    response.Headers.Location = Request.RequestUri;

                    return response;
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, location);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.InnerException);
            }
        }
    }
}
