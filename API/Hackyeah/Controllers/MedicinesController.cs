﻿using Hackyeah.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hackyeah.Controllers
{
    public class MedicinesController : ApiController
    {
        private DBConnection DB = new DBConnection();

        //GET /api/medicines
        public HttpResponseMessage GetMedicines()
        {
            List<Medicine> medicineList = new List<Medicine>();
            medicineList = DB.Medicines.ToList();
            HttpResponseMessage response;
            response = Request.CreateResponse(HttpStatusCode.OK, medicineList);

            return response;
        }

        //GET /api/medicines/{id}
        public HttpResponseMessage GetMedicineById(int id)
        {
            List<Medicine> medicineList = new List<Medicine>();
            medicineList = DB.Medicines.Where(p => p.Id == id).ToList();
            HttpResponseMessage response;


            if (medicineList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, medicineList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Location not found.");
            }

            return response;
        }

        //GET /api/medicines?patientId={patientId}
        public HttpResponseMessage GetMedicinesByUserId(int patientId)
        {
            List<Medicine> medicineList = new List<Medicine>();
            medicineList = DB.Medicines.Where(p => p.PatientId == patientId).ToList();
            HttpResponseMessage response;


            if (medicineList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, medicineList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Location for user not found.");
            }

            return response;
        }

        //POST /api/medicines
        [HttpPost]
        public HttpResponseMessage AddNewMedicine(Medicine medicine)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DB.Medicines.Add(medicine);
                    DB.SaveChanges();

                    var response = Request.CreateResponse(HttpStatusCode.Created, medicine);
                    response.Headers.Location = Request.RequestUri;

                    return response;
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, medicine);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.InnerException);
            }
        }
    }
}
