﻿using Hackyeah.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hackyeah.Controllers
{
    public class PatientsController : ApiController
    {
        private DBConnection DB = new DBConnection();

        //GET /api/patients
        public HttpResponseMessage GetPatients()
        {
            List<Patient> patientList = new List<Patient>();
            patientList = DB.Patients.ToList();
            HttpResponseMessage response;
            response = Request.CreateResponse(HttpStatusCode.OK, patientList);

            return response;
        }

        //GET /api/patients/{id}
        public HttpResponseMessage GetPatientById(int id)
        {
            List<Patient> patientList = new List<Patient>();
            patientList = DB.Patients.Where(p => p.Id == id).ToList();
            HttpResponseMessage response;

            if (patientList.Count() > 0)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, patientList);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Patient not found.");
            }

            return response;
        }

        //POST /api/patients
        [HttpPost]
        public HttpResponseMessage AddNewPatient(Patient patient)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DB.Patients.Add(patient);
                    DB.SaveChanges();

                    var response = Request.CreateResponse(HttpStatusCode.Created, patient);
                    response.Headers.Location = Request.RequestUri;

                    return response;
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, patient);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.InnerException);
            }
        }
    }
}