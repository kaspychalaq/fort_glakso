//
//  CarerApi.swift
//  FortGlaksoMobile
//
//  Created by Kasper on 11/24/18.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

final class ApiManager {

    let url: String = "http://kiniak94-001-site1.ctempurl.com/api"

    func loginCarer(forUserName userName: String, forUserPassword password: String, successCompletion: @escaping (Response) -> Void, errorCompletion: @escaping (String) -> Void){

        let params = ["UserName": userName, "Password": password]

        let request = URLRequest(baseUrl: url, path: "/carers", method: RequestMethod.get, params: params)

        // Sending request to the server.
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Parsing incoming data
            if let error = error {
                errorCompletion(error.localizedDescription)
            }
            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {

                    guard let data = data else {
                        errorCompletion("No data")
                        return
                    }
                    do{
                        let json = try JSONDecoder().decode(Response.self, from: data)
                        successCompletion(json)
                    } catch {
                        errorCompletion("Parsing failed!")
                    }
                } else {
                    errorCompletion("Not authorized")
                }
            } else {
                errorCompletion("No response")
            }
        }
        task.resume()
    }

    func registerCarer(forUserName userName: String, forUserPassword password: String, successCompletion: @escaping (Response) -> Void, errorCompletion: @escaping (String) -> Void){

        let params = ["UserName": userName, "Password": password]

        let request = URLRequest(baseUrl: url, path: "/carers", method: RequestMethod.post, params: params)

        // Sending request to the server.
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Parsing incoming data
            if let error = error {
                errorCompletion(error.localizedDescription)
            }
            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {

                    guard let data = data else {
                        errorCompletion("No data")
                        return
                    }
                    do{
                        let json = try JSONDecoder().decode(Response.self, from: data)
                        successCompletion(json)
                    } catch {
                        errorCompletion("Parsing failed!")
                    }
                } else {
                    errorCompletion("Not authorized")
                }
            } else {
                errorCompletion("No response")
            }
        }
        task.resume()
    }

    func loginPatient(forQrId qrId: String, successCompletion: @escaping (Response) -> Void, errorCompletion: @escaping (String) -> Void){

        let params = ["QrId": qrId]

        let request = URLRequest(baseUrl: url, path: "/patients", method: RequestMethod.get, params: params)

        // Sending request to the server.
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Parsing incoming data
            if let error = error {
                errorCompletion(error.localizedDescription)
            }
            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {

                    guard let data = data else {
                        errorCompletion("No data")
                        return
                    }
                    do{
                        let json = try JSONDecoder().decode(Response.self, from: data)
                        successCompletion(json)
                    } catch {
                        errorCompletion("Parsing failed!")
                    }
                } else {
                    errorCompletion("Not authorized")
                }
            } else {
                errorCompletion("No response")
            }
        }
        task.resume()
    }

    func registerPatient(forName name: String, forSurname surname: String, forBirthdate birthDate: String, forPersonId personId: Int, forBloodType bloodType: String, forDeviceId deviceId: String, successCompletion: @escaping (Response) -> Void, errorCompletion: @escaping (String) -> Void){

        let params = ["Name": name, "Surname": surname, "BirthDate": birthDate, "PersonId": personId, "BloodType": bloodType, "DeviceId": deviceId] as [String : Any]

        let request = URLRequest(baseUrl: url, path: "/patients", method: RequestMethod.post, params: params)

        // Sending request to the server.
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Parsing incoming data
            if let error = error {
                errorCompletion(error.localizedDescription)
            }
            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {

                    guard let data = data else {
                        errorCompletion("No data")
                        return
                    }
                    do{
                        let json = try JSONDecoder().decode(Response.self, from: data)
                        successCompletion(json)
                    } catch {
                        errorCompletion("Parsing failed!")
                    }
                } else {
                    errorCompletion("Not authorized")
                }
            } else {
                errorCompletion("No response")
            }
        }
        task.resume()
    }

    func getAddresses(successCompletion: @escaping ([Addresses]) -> Void, errorCompletion: @escaping (String) -> Void){

        let params = [String:Any]()

        let request = URLRequest(baseUrl: url, path: "/addresses", method: RequestMethod.get, params: params)

        // Sending request to the server.
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Parsing incoming data
            if let error = error {
                errorCompletion(error.localizedDescription)
            }
            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {

                    guard let data = data else {
                        errorCompletion("No data")
                        return
                    }
                    do{
                        let json = try JSONDecoder().decode([Addresses].self, from: data)
                        successCompletion(json)
                    } catch {
                        errorCompletion("Parsing failed!")
                    }
                } else {
                    errorCompletion("Not authorized")
                }
            } else {
                errorCompletion("No response")
            }
        }
        task.resume()
    }

    func getLocations(successCompletion: @escaping ([Locations]) -> Void, errorCompletion: @escaping (String) -> Void){

        let params = [String:Any]()

        let request = URLRequest(baseUrl: url, path: "/locations", method: RequestMethod.get, params: params)

        // Sending request to the server.
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Parsing incoming data
            if let error = error {
                errorCompletion(error.localizedDescription)
            }
            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {

                    guard let data = data else {
                        errorCompletion("No data")
                        return
                    }
                    do{
                        let json = try JSONDecoder().decode([Locations].self, from: data)
                        successCompletion(json)
                    } catch {
                        errorCompletion("Parsing failed!")
                    }
                } else {
                    errorCompletion("Not authorized")
                }
            } else {
                errorCompletion("No response")
            }
        }
        task.resume()
    }

    func getMedicines(successCompletion: @escaping ([Medicines]) -> Void, errorCompletion: @escaping (String) -> Void){

        let params = [String:Any]()

        let request = URLRequest(baseUrl: url, path: "/medicines", method: RequestMethod.get, params: params)

        // Sending request to the server.
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Parsing incoming data
            if let error = error {
                errorCompletion(error.localizedDescription)
            }
            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {

                    guard let data = data else {
                        errorCompletion("No data")
                        return
                    }
                    do{
                        let json = try JSONDecoder().decode([Medicines].self, from: data)
                        successCompletion(json)
                    } catch {
                        errorCompletion("Parsing failed!")
                    }
                } else {
                    errorCompletion("Not authorized")
                }
            } else {
                errorCompletion("No response")
            }
        }
        task.resume()
    }

    func getDispensers(successCompletion: @escaping ([Dispensers]) -> Void, errorCompletion: @escaping (String) -> Void){

        let params = [String:Any]()

        let request = URLRequest(baseUrl: url, path: "/medicines", method: RequestMethod.get, params: params)

        // Sending request to the server.
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Parsing incoming data
            if let error = error {
                errorCompletion(error.localizedDescription)
            }
            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {

                    guard let data = data else {
                        errorCompletion("No data")
                        return
                    }
                    do{
                        let json = try JSONDecoder().decode([Dispensers].self, from: data)
                        successCompletion(json)
                    } catch {
                        errorCompletion("Parsing failed!")
                    }
                } else {
                    errorCompletion("Not authorized")
                }
            } else {
                errorCompletion("No response")
            }
        }
        task.resume()
    }
}
