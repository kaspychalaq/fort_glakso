//
//  File.swift
//  FortGlaksoMobile
//
//  Created by Kasper on 11/24/18.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

struct Patients: Codable {
    var name: String?
    var surname: String?
    var birthDate: String?
    var personId: Int?
    var bloodType: String?
    var deviceId: String?
    var phoneNo: String?

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case surname = "Surname"
        case birthDate = "BirthDate"
        case personId = "PersonId"
        case bloodType = "BloodType"
        case deviceId = "DeviceId"
    }
}
