//
//  Medicines.swift
//  FortGlaksoMobile
//
//  Created by Kasper on 11/25/18.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

struct Medicines: Codable {
    var name: String?
    var patientId: Int?
    var time: String?
    var dose: Int?

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case patientId = "PatientId"
        case time = "Time"
        case dose = "Dose"
    }
}
