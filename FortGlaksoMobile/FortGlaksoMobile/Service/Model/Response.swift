//
//  Response.swift
//  FortGlaksoMobile
//
//  Created by Kasper on 11/24/18.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

struct Response: Codable {
    var status: Int?
    var message: String?

    enum CodingKeys: String, CodingKey {
        case status = "Status"
        case message = "Message"
    }
}
