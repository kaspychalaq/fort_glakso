//
//  Dispensers.swift
//  FortGlaksoMobile
//
//  Created by Kasper on 11/25/18.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

struct Dispensers: Codable {
    var id: Int?
    var patientId: Int?
    var status: Bool?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case patientId = "PatientId"
        case status = "Status"
    }
}
