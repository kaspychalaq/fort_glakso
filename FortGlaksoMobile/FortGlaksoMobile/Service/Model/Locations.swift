//
//  Locations.swift
//  FortGlaksoMobile
//
//  Created by Kasper on 11/25/18.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

struct Locations: Codable {
    var patientId: Int?
    var latitude: String?
    var longitude: String?
    var date: String?

    enum CodingKeys: String, CodingKey {
        case patientId = "PatientId"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case date = "Date"
    }
}
