//
//  PatientsCarers.swift
//  FortGlaksoMobile
//
//  Created by Kasper on 11/24/18.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

struct PatientsCarers: Codable {
    var patientId: Int?
    var carerId: Int?

    enum CodingKeys: String, CodingKey {
        case patientId = "PatientId"
        case carerId = "CarrerId"
    }
}
