//
//  Carers.swift
//  FortGlaksoMobile
//
//  Created by Kasper on 11/24/18.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

struct Carers: Codable {
    var userName: String?
    var password: String?
    var firstName: String?
    var lastName: String?
    var phoneNumber: String?

    enum CodingKeys: String, CodingKey {
        case userName = "UserName"
        case password = "Password"
    }

}
