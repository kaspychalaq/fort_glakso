//
//  Addresses.swift
//  FortGlaksoMobile
//
//  Created by Kasper on 11/25/18.
//  Copyright © 2018 GSK. All rights reserved.
//

import Foundation

struct Addresses: Codable {
    var id: Int?
    var patientId: Int?
    var name: String?
    var latitude: String?
    var longitude: String?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case patientId = "PatientId"
        case name = "Name"
        case latitude = "Latitude"
        case longitude = "Longitude"
    }
}
