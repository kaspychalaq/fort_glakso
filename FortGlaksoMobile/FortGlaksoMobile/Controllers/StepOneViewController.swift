//
//  StepOneViewController.swift
//  FortGlaksoMobile
//
//  Created by Mikolaj Bujok on 25/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import UIKit

class StepOneViewController: UIViewController {


    @IBOutlet weak var phoneNumberLabel: UITextField!
    
    let apiManager = ApiManager()
    var carer = Carers()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberLabel.delegate = self
        print(carer)
//        if let firstName = firstNameLabel.text, let lastName = lastNameLabel.text, let email = emailLabel.text {
////            print("\(firstName) \(lastName) \(email)")
////            apiManager.registerCarer(forUserName: firstName, forUserPassword: <#T##String#>, successCompletion: <#T##(Response) -> Void#>, errorCompletion: <#T##(String) -> Void#>)
//        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        if let phoneNo = phoneNumberLabel.text {
            carer.phoneNumber = phoneNo
        }
        if let destinationVc = storyboard?.instantiateViewController(withIdentifier: "stepTwoVc") as? StepTwoViewController {
            destinationVc.carer = carer
            present(destinationVc, animated: true, completion: nil)
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StepOneViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
