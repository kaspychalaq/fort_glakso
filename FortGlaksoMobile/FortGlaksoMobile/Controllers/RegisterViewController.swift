//
//  RegisterViewController.swift
//  FortGlaksoMobile
//
//  Created by Mikolaj Bujok on 25/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var passwordLabel: UITextField!
    
    @IBOutlet weak var passwordConfirmation: UITextField!
    
    @IBOutlet weak var infoLabel: UILabel!
    var carer = Carers()
    
    
    let apiManager = ApiManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailLabel.delegate = self
        passwordLabel.delegate = self
        passwordConfirmation.delegate = self

//            apiManager.registerCarer(forUserName: firstName, forUserPassword: <#T##String#>, successCompletion: <#T##(Response) -> Void#>, errorCompletion: <#T##(String) -> Void#>)
//        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        if passwordLabel.text == passwordConfirmation.text {
            //TODO: Segue
            carer.userName = emailLabel.text
            carer.password = passwordLabel.text
            
            if let destinationVc = UIStoryboard(name: "Registration", bundle: nil).instantiateViewController(withIdentifier: "stepOneVC") as? StepOneViewController {
                destinationVc.carer = carer
                present(destinationVc, animated: true, completion: nil)
            }
        } else {
            infoLabel.text = "Provided passwords doesn't match."
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RegisterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
