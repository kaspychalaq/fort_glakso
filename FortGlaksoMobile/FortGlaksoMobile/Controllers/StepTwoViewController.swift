//
//  StepTwoViewController.swift
//  FortGlaksoMobile
//
//  Created by Mikolaj Bujok on 25/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import UIKit

class StepTwoViewController: UIViewController {

    var carer = Carers()
    var patient = Patients()
    @IBOutlet weak var firstNameLabel: UITextField!
    @IBOutlet weak var lastNameLabel: UITextField!
    @IBOutlet weak var PhoneNumberLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNameLabel.delegate = self
//        lastNameLabel.delegate
        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        if let name = firstNameLabel.text, let lastName = lastNameLabel.text, let phoneNo = PhoneNumberLabel.text {
            patient.name = name
            patient.surname = lastName
            patient.phoneNo = phoneNo
        }
        if let destinationVc = storyboard?.instantiateViewController(withIdentifier: "stepThreeVc") as? StepThreeViewController {
            destinationVc.carer = carer
            destinationVc.patient = patient
            present(destinationVc, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StepTwoViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
