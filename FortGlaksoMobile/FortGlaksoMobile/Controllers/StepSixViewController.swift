//
//  StepSixViewController.swift
//  FortGlaksoMobile
//
//  Created by Mikolaj Bujok on 25/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import UIKit

class StepSixViewController: UIViewController {

    
    var apiManager = ApiManager()
    var carer = Carers()
    var patient = Patients()
    var address = Addresses()
    var isCarerComplete: Bool? {
        didSet {
            if isPatientComplete == true {
                let destinationVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainVc")
                    present(destinationVc, animated: true, completion: nil)
            }
        }
    }
    var isPatientComplete: Bool?
    
    @IBOutlet weak var infoLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        apiManager.registerCarer(forUserName: carer.firstName!, forUserPassword: carer.password!, successCompletion: registerCarerSuccessHandler, errorCompletion: registerCarerErrorHandler)
        apiManager.registerPatient(forName: patient.name!, forSurname: patient.surname!, forBirthdate: "", forPersonId: 100, forBloodType: "", forDeviceId: "", successCompletion: registerPatientSuccessHandler, errorCompletion: registerPatientErrorHandler)
    }
    
    func registerCarerSuccessHandler(response: Response) {
        isCarerComplete = true
    }
    func registerPatientSuccessHandler(response: Response) {
        isCarerComplete = true
    }
    func registerCarerErrorHandler(text: String) {
        isCarerComplete = false
        infoLabel.text = "Sorry, there was an internet connection, please try again!"
    }
    func registerPatientErrorHandler(text: String) {
        isCarerComplete = false
        infoLabel.text = "Sorry, there was an internet connection, please try again!"
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
