//
//  StepThreeViewController.swift
//  FortGlaksoMobile
//
//  Created by Mikolaj Bujok on 25/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import UIKit

class StepThreeViewController: UIViewController {

    
    @IBOutlet weak var addressTextField: UITextField!
    
    var carer = Carers()
    var patient = Patients()
    var address = Addresses()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        if let addressName = addressTextField.text {
            address.name = addressName
        }
        if let destinationVc = storyboard?.instantiateViewController(withIdentifier: "stepFourVc") as? StepFourViewController {
            destinationVc.carer = carer
            destinationVc.patient = patient
            destinationVc.address = address
            present(destinationVc, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
