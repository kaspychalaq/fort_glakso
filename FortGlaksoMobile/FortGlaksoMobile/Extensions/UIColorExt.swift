//
//  UIColorExt.swift
//  FortGlaksoMobile
//
//  Created by Mikolaj Bujok on 25/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import UIKit

extension UIColor {

    static var blue = UIColor(red: 13 / 255, green: 158 / 255, blue: 249 / 255, alpha: 1)
    static var turquoise = UIColor(red: 0 / 255, green: 230 / 255, blue: 228 / 255, alpha: 1)
    static var green = UIColor(red: 153 / 255, green: 226 / 255, blue: 101 / 255, alpha: 1)
    static var orange = UIColor(red: 250 / 255, green: 166 / 255, blue: 25 / 255, alpha: 1)
}
