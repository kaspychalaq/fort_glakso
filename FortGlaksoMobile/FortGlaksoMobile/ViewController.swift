//
//  ViewController.swift
//  FortGlaksoMobile
//
//  Created by Kasper on 11/24/18.
//  Copyright © 2018 GSK. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

//    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        username.delegate = self
        password.delegate = self
    }

    @IBAction func onButtonClick(_ sender: Any) {


//        ApiManager().getDispensers(successCompletion: fetchDispensersSuccess, errorCompletion: fetchDispensersFail)
        //ApiManager().getMedicines(successCompletion: fetchMedicinesSuccess, errorCompletion: fetchMedicinesFail)
        //ApiManager().getLocations(successCompletion: fetchLocationsSuccess, errorCompletion: fetchLocationsFail)
        //ApiManager().getAddresses(successCompletion: fetchAdressesSuccess, errorCompletion: fetchAdressesFail)

        if let user = username.text, let pass = password.text {
        ApiManager().loginCarer(forUserName: user, forUserPassword: pass, successCompletion: loginSuccess, errorCompletion: loginFailed)
        }
    }

    func fetchDispensersSuccess(dispensers: [Dispensers]){
        print(dispensers)
    }

    func fetchDispensersFail(error: String){
        print(error)
    }

    func fetchMedicinesSuccess(medicines: [Medicines]){
        print(medicines)
    }

    func fetchMedicinesFail(error: String){
        print(error)
    }

    func fetchLocationsSuccess(locations: [Locations]){
        print(locations)
    }

    func fetchLocationsFail(error: String){
        print(error)
    }

    func fetchAdressesSuccess(adresses: [Addresses]){
        print(adresses)
    }

    func fetchAdressesFail(error: String){
        print(error)
    }

    func loginSuccess(response: Response){
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainVC")
        self.present(vc, animated: true, completion: nil)
    }

    func loginFailed(error: String){


    }

}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
