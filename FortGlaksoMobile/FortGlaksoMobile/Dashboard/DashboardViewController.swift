//
//  DashboardViewController.swift
//  FortGlaksoMobile
//
//  Created by Konrad on 24/11/2018.
//  Copyright © 2018 GSK. All rights reserved.
//

import CoreData
import CoreLocation
import MapKit
import UIKit

class DashboardViewController: UIViewController {
    
    private let locationManager = LocationManager.shared
    private var locationList: [CLLocation] = []
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private var locations = [Location]()
    private var timer: Timer?
    @IBOutlet weak var wardimage: UIImageView!
    @IBOutlet weak var wardName: UILabel!

    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var transclucentView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startLocationUpdates()
        mapView.delegate = self
        prepareMap()
        prepareImage()
        // Do any additional setup after loading the view.
    }
    
    func prepareMap() {
        mapView.layer.cornerRadius = mapView.bounds.width / 2
        let gestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(mapTapped(apGestureRecogniser:)))
        mapView.addGestureRecognizer(gestureRecogniser)
    }

    func prepareImage() {
        wardimage.layer.cornerRadius = wardimage.bounds.width / 2
        wardimage.clipsToBounds = true
    }
    
    @objc func mapTapped(apGestureRecogniser: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: .curveEaseInOut, animations: {
//                        self.mapView.transform = CGAffineTransform(translationX: -16, y: 0)
                        self.mapView.frame = CGRect(x: 0, y: 200, width: self.view.frame.width, height: self.view.frame.height - 300)
                        self.transclucentView.isHidden = false
        }, completion: nil)
    }
    
    private func saveLocation() {
        let context = appDelegate.persistentContainer.viewContext
        
        for location in locationList {
            let locationObject = Location(context: context)
            locationObject.timestamp = location.timestamp
            locationObject.latitude = location.coordinate.latitude
            locationObject.longitude = location.coordinate.longitude
        }
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }

    private func startLocationUpdates() {
        locationManager.delegate = self
        locationManager.activityType = .fitness
        locationManager.distanceFilter = 10
        locationManager.startUpdatingLocation()
    }
    
    private func mapRegion() -> MKCoordinateRegion? {
        let latitudes = locations.map { location -> Double in
            return location.latitude
        }
        
        let longitudes = locations.map { location -> Double in
            return location.longitude
        }
        let maxLat = latitudes.max()!
        let minLat = latitudes.min()!
        let maxLong = longitudes.max()!
        let minLong = longitudes.min()!
        
        let center = CLLocationCoordinate2D(latitude: (minLat + maxLat) / 2,
                                            longitude: (minLong + maxLong) / 2)
        let span = MKCoordinateSpan(latitudeDelta: (maxLat - minLat) * 1.3,
                                    longitudeDelta: (maxLong - minLong) * 1.3)
        return MKCoordinateRegion(center: center, span: span)
    }
    private func polyLine() -> MKPolyline {
        let coords: [CLLocationCoordinate2D] = locations.map { location in
            return CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        }
        return MKPolyline(coordinates: coords, count: coords.count)
    }
    
    private func loadMap() {
        
        guard
            let region = mapRegion(),
            locations.count > 0
            else {
                let alert = UIAlertController(title: "Error",
                                              message: "Sorry, this run has no locations saved",
                                              preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                present(alert, animated: true)
                return
        }
        
        mapView.setRegion(region, animated: true)
            mapView.addOverlay(polyLine())
    }
    
    @IBAction func stopUpdating(_ sender: Any) {
        locationManager.stopUpdatingLocation()
        saveLocation()
        fetchData()
        DispatchQueue.main.async {
            self.loadMap()
        }
    }
    
    func fetchData() {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Location")
        let context = appDelegate.persistentContainer.viewContext
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let location = data as! Location
                locations.append(location)
            }
            
        } catch {
            
            print("Failed")
        }
        print(locations)
    }
    
}

extension DashboardViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for newLocation in locations {
            let howRecent = newLocation.timestamp.timeIntervalSinceNow
            guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else { continue }
            locationList.append(newLocation)
        }
    }
}

extension DashboardViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let polyline = overlay as? MKPolyline else {
            return MKOverlayRenderer(overlay: overlay)
        }
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = .black
        renderer.lineWidth = 3
        return renderer
    }
}
